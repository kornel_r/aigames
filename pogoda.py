import pandas as pd
import numpy as np
import geopy.distance
import scipy

lats = np.load('lats.npy')
lngs = np.load('lngs.npy')

def distanceFun(point1, point2):
    return geopy.distance.geodesic(point1, point2).km

def search(startX, startY, lat, long, data, forecastHour):
    fileName = 'weather_and_winds/forecast'+forecastHour+'_'+data+'.npz'
    file = np.load(fileName)
    dice = np.dstack((lats, lngs, file['cloud_echo_tops'], file['vertically_integrated_liquid_water'], file['vertically_integrated_liquid_water'],
                     file['flight_level_25000_u'], file['flight_level_25000_v'], file['flight_level_30000_u'], file['flight_level_30000_v'],
                     file['flight_level_50000_u'], file['flight_level_50000_v']))
    
    step = 1
    minValue = 5
    currentValue = 10000
    stepNumber = 0
    maxStepNumber = 5000
    x = startX
    y = startY
    
    while (currentValue > minValue) and (stepNumber <= maxStepNumber):
        points = [[x, y+step], [x, y-step], [x+step, y+step], [x+step, y], [x, y-step], [x-step, y+step], [x-step, y], [x-step, y-step]]
        for point in points:
            if point[0] >= 0 and point[0] <= 151 and point[1] >= 0 and point[1] <= 256:
                value = distanceFun((lat,long),(dice[point[0]][point[1]][0], dice[point[0]][point[1]][1]))
                if value < currentValue:
                    x = point[0]
                    y = point[1]
                    currentValue = value
        stepNumber += 1
    #print(currentValue)    
    return dice[x][y]