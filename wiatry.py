import numpy as np
import json
import pandas as pd
from datetime import datetime, timedelta
import geopy.distance
from pogoda import distanceFun
from prognoza import *

def search(startX, startY, lat, long, dice):

    step = 1
    minValue = 5
    currentValue = 10000
    stepNumber = 0
    maxStepNumber = 5000
    x = startX
    y = startY
    
    while (currentValue > minValue) and (stepNumber <= maxStepNumber):
        points = [[x, y+step], [x, y-step], [x+step, y+step], [x+step, y], [x, y-step], [x-step, y+step], [x-step, y], [x-step, y-step]]
        for point in points:
            if point[0] >= 0 and point[0] <= 151 and point[1] >= 0 and point[1] <= 256:
                value = distanceFun((lat,long),(dice[point[0]][point[1]][0], dice[point[0]][point[1]][1]))
                if value < currentValue:
                    x = point[0]
                    y = point[1]
                    currentValue = value
        stepNumber += 1
       
    return dice[x][y]

def alt_level_func(altitude):
    if altitude < 27500:
        return "25000"
    elif (altitude >= 27500) and (altitude < 40000):
        return "30000"
    else:
        return "50000"  
    
def flight_angles(fixes):
    fixes = np.array([fixes])
    flyAngles = np.array([])
    for i in range(fixes.shape[1]-1):
        vector1 = np.array([])
        vector1 = np.append(vector1, fixes[0][i][0]-fixes[0][i+1][0])
        vector1 = np.append(vector1, fixes[0][i][1]-fixes[0][i+1][1])
        a = np.arctan(vector1[1]/vector1[0])
        flyAngles = np.append(flyAngles, a)
    
    return flyAngles
    #windAngle = np.arcsin(forecastFile[]/(result[8]**2+result[9]**2))
    
def wind_angle(time, altitude_level, fixes):
    altitude = alt_level_func(altitude_level)
    date = datetime.strftime(time,"%Y-%m-%d_%H.%M.%S")
    fileName = 'weather_and_winds/forecast'+'0'+'_'+date+'.npz'
    forecastFile = np.load(fileName)  # file with weather forecast
    dice = np.dstack((forecastFile['flight_level_'+altitude+"_u"],
                    forecastFile['flight_level_'+altitude+"_v"]))
    fixes = np.array([fixes])
    windAngles = np.array([])
    for i in range(fixes.shape[1]-1):
        coord = search(150, 150, fixes[0][i][0], fixes[0][i][1], dice)
        windAngles = np.append(windAngles, np.arcsin(coord[1]/(coord[0]**2+coord[1]**2)))
    return windAngles
      
if __name__ == "__main__":
    df = pd.read_csv('flight_plans_train.csv')
    df = preprocess(df)
    df["angle"] = df.apply(lambda row: flight_angles(row["decoded_fixes"]), axis=1)
#    df["angle_wind"] = df.apply(lambda row: wind_angle(row['new_date'],row["assigned_altitude"],row['decoded_fixes']), axis=1)
#    df['angles_diff'] = df['angle'] - df['angle_wind']
