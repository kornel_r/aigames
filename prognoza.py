import numpy as np
import json
import pandas as pd
from datetime import datetime, timedelta
import geopy.distance
from pogoda import (distanceFun, search)


def hour_rounder(t):
    # Rounds to nearest hour by adding a timedelta hour if minute >= 30
    return (t.replace(second=0, microsecond=0, minute=0, hour=t.hour)
               +timedelta(hours=t.minute//30))
    
def transform_time(x):
    time_ = datetime.strptime(x, "%Y-%m-%d %H:%M:%S %Z")
    time_ = hour_rounder(time_)
    return time_

def transform_fixes(x):
    float_fixes = []
    temp = x.split('[')
    temp = temp[2:]
    for element in temp:
        element = element[0:-3]
        first,second = element.split(', ')
        first = float(first)
        second = float(second)
        coordinates = [first,second]
        float_fixes.append(coordinates)
    return float_fixes

def last(x):
    return x[-1]

def first(x):
    return x[0]

def round_flight_time(x):
    if x <= 6:
        return round(x)
    else:
        return 6

#def estimate_time(distance, speed):
#    distance = distance

def preprocess(df):
    df['new_date'] = df['departure_time'].apply(transform_time)
    df["decoded_fixes"] = df["decoded_fixes"].apply(transform_fixes)
    df['start'] = df['decoded_fixes'].apply(first)
    df['stop'] = df['decoded_fixes'].apply(last)
    df['distance'] = df.apply(lambda row: distanceFun(row['start'], row['stop']), axis=1)
    df['time_est'] = df["distance"] / df['requested_airspeed']
    df['time_est'] = df['time_est'].apply(round_flight_time).astype(str)
    return df

def predict_weather(coord_points, date, ):
    with open("new_airports.json") as json_file:
        data = json.load(json_file)
    
    for i in range(len(coord_points)):
        for coord in coord_points:
            pogoda = search()
            (coord[i][0], coord[i][1], datetime.strftime(time,"%Y-%m-%d_%H.%M.%S"),"0")
    
    
if __name__ == "__main__":
   df = pd.read_csv('flight_plans_train.csv')
   df = preprocess(df)

